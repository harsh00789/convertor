<?php

namespace Convertor\Weight\Milligram;

/**
*
*/
class Milligram
{
    


    /* These all are Milligram to other conversion functions */

    public function MilligramToKilogram($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*0.000001;
        }
    }
    public function MilligramToGram($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*0.001;
        }
    }

    public function MilligramToMetricton($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*9.999999999E-10;
        }
    }

    public function MilligramToLongton($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*9.842073304E-10;
        }
    }

    public function MilligramToShortton($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*1.10231221E-9;
        }
    }

    public function MilligramToPound($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*0.0000022046;
        }
    }
    public function Validate($num1)
    {
        
        if ((is_numeric($num1) || is_double($num1))) {
            return 1;
        } else {
            throw new \InvalidArgumentException("Input type is not integer or double", 1);
        }
    }
}
