<?php

namespace Convertor\Weight\Kilogram;

/**
*
*/
class Kilogram
{



    /* These all are Kilogram to other conversion functions */

    public function KilogramToGram($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*1000;
        }
    }

    public function KilogramToMilligram($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*1000000;
        }
    }

    public function KilogramToMetricton($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*0.001;
        }
    }

    public function kilogramTolongton($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*0.0009842073;
        }
    }

    public function KilogramToShortton($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*0.0011023122;
        }
    }

    public function KilogramToPound($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*2.2046244202;
        }
    }
    public function Validate($num1)
    {
        
        if ((is_numeric($num1) || is_double($num1))) {
            return 1;
        } else {
            throw new \InvalidArgumentException("Input type is not integer or double", 1);
        }
    }
}
