<?php

namespace Convertor\Weight\Gram;

/**
*
*/
class Gram
{
    


    /* These all are Gram to other conversion functions */

    public function GramToKilogram($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*0.001;
        }
    }

    public function GramToMilligram($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*1000;
        }
    }

    public function GramToMetricton($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*0.000001;
        }
    }

    public function GramTolongton($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*9.842073304E-7;
        }
    }

    public function GramToShortton($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*0.0000011023;
        }
    }

    public function GramToPound($num1)
    {
        
        if (Validate(num1)==1) {
            return $num1*0.0022046244;
        }
    }
    public function Validate($num1)
    {
        
        if ((is_numeric($num1) || is_double($num1))) {
            return 1;
        } else {
            throw new \InvalidArgumentException("Input type is not integer or double", 1);
        }
    }
}
