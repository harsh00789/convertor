<?php

namespace Convertor\Length\KiloMeter;

/**
*
*/
class Kilometer
{



/* These all are KiloMeter to other conversion functions */

    public function KilometerToMeter($num1)
    {
        
        if ($this->Validate($num1)==1) {
            return $num1*1000;
        }
    }

    public function KilometerToCentimeter($num1)
    {
        
        if ($this->Validate($num1)==1) {
            return $num1*100000;
        }
    }

    public function KilometerToMillimeter($num1)
    {
        
        if ($this->Validate($num1)==1) {
            return $num1*1000000;
        }
    }

    public function KilometerToMile($num1)
    {
        
        if ($this->Validate($num1)==1) {
            return $num1*0.6213688756;
        }
    }

    public function KilometerToYard($num1)
    {
        
        if ($this->Validate($num1)==1) {
            return $num1*1093.6132983;
        }
    }

    public function KilometerToFoot($num1)
    {
        
        if ($this->Validate($num1)==1) {
            return $num1*3280.839895  ;
        }
    }

    public function KilometerToInch($num1)
    {
        
        if ($this->Validate($num1)==1) {
            return $num1*39370.07874;
        }
    }
    private function Validate($num1)
    {
        
        if ((is_numeric($num1) || is_double($num1))) {
            return 1;
        } else {
            throw new \InvalidArgumentException("Input type is not integer or double", 1);
        }
    }
}
