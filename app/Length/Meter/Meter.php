<?php

namespace Convertor\Length\Meter;

/**
*
*/
class Meter
{
    


    /* These all are meter to other conversion functions */

    public function MeterToKilometer($num1)
    {
        
        if ($this->Validate($num1)==1) {
            return $num1*0.001;
        }
    }

    public function MeterToCentimeter($num1)
    {
        
        if ($this->Validate($num1)==1) {
            return $num1*100;
        }
    }

    public function MeterToMillimeter($num1)
    {
        
        if ($this->Validate($num1)==1) {
            return $num1*1000;
        }
    }

    public function MeterToMile($num1)
    {
         
        
        if ($this->Validate($num1)==1) {
            return $num1*0.0006213689;
        }
    }

    public function MeterToYard($num1)
    {
         
        
        if ($this->Validate($num1)==1) {
            return $num1*1.0936132983;
        }
    }

    public function MeterToFoot($num1)
    {
        
        if ($this->Validate($num1)==1) {
            return $num1*3.280839895;
        }
    }

    public function MeterToInch($num1)
    {
        
        if ($this->Validate($num1)==1) {
            return $num1*39.37007874;
        }
    }
    private function Validate($num1)
    {
        
        if ((is_numeric($num1) || is_double($num1))) {
            return 1;
        } else {
            throw new \InvalidArgumentException("Input type is not integer or double", 1);
        }
    }
}
