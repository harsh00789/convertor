<?php

namespace Convertor\Length\Centimeter;

/**
*
*/
class Centimeter
{
    


    /* These all are centimeter to other conversion functions */

    public function CentimeterToKilometer($num1)
    {
        
        if ($this->Validate($num1)==true) {
            return $num1*0.00001;
        }
    }

    public function CentimeterToMeter($num1)
    {
        
        if ($this->Validate($num1)==true) {
            return $num1*0.01;
        }
    }

    public function CentimeterToMillimeter($num1)
    {
        
        if ($this->Validate($num1)==true) {
            return $num1*10;
        }
    }

    public function CentimeterToMile($num1)
    {
        
        if ($this->Validate($num1)==true) {
            return $num1*0.0000062137;
        }
    }

    public function CentimeterToYard($num1)
    {
        
        if ($this->Validate($num1)==true) {
            return $num1*0.010936133;
        }
    }

    public function CentimeterToFoot($num1)
    {
        
        if ($this->Validate($num1)==true) {
            return $num1*0.032808399;
        }
    }

    public function CentimeterToInch($num1)
    {
        
        if ($this->Validate($num1)==true) {
            return $num1*0.3937007874;
        }
    }
    private function Validate($num1)
    {
        
        if ((is_numeric($num1) || is_double($num1))) {
            return true;
        } else {
            throw new \InvalidArgumentException("Input type is not integer or double", 1);
        }
    }
}

