<?php

namespace Tests\WeightTests\GramTests;

class WeightGramToShorttonConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConvertInteger()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToShortton(2);

        $this->assertEquals(0.0000022046, $result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToShortton(2.5);

        $this->assertEquals(0.0000027558, $result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToShortton(0);

        $this->assertEquals(0, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToShortton(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToShortton("two");
    }
}
