<?php

namespace Tests\WeightTests\GramTests;

class WeightGramToMetrictonConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConvertInteger()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToMetricton(2);

        $this->assertEquals(0.000002, $result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToMetricton(2.5);

        $this->assertEquals(0.0000025, $result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToMetricton(0);

        $this->assertEquals(0, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToMetricton(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToMetricton("two");
    }
}
