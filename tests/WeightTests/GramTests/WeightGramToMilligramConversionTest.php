<?php

namespace Tests\WeightTests\GramTests;

class WeightGramToMilligramConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConvertInteger()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToMilligram(2);

        $this->assertEquals(2000, $result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToMilligram(2.5);

        $this->assertEquals(2500, $result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToMilligram(0);

        $this->assertEquals(0, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToMilligram(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToMilligram("two");
    }
}
