<?php

namespace Tests\WeightTests\GramTests;

class WeightGramToLongtonConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConvertInteger()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToLongton(2);

        $this->assertEquals(0.0000019684, $result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToLongton(2.5);

        $this->assertEquals(0.0000024605, $result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToLongton(0);

        $this->assertEquals(0, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToLongton(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToLongton("two");
    }
}
