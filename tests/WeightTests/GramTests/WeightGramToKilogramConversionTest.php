<?php

namespace Tests\WeightTests\GramTests;

class WeightGramToKilogramConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConvertInteger()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToKilogram(2);

        $this->assertEquals(0.002, $result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToKilogram(2.5);

        $this->assertEquals(0.0025, $result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToKilogram(0);

        $this->assertEquals(0, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToKilogram(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Weight\Gram\Gram();

        $result = $operation->GramToKilogram("two");
    }
}
