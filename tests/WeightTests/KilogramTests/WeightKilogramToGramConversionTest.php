<?php

namespace Tests\WeightTests\KilogramTests;

class WeightKilogramToGramConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConvertInteger()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToGram(2);

        $this->assertEquals(2000, $result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToGram(2.5);

        $this->assertEquals(2500, $result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToGram(0);

        $this->assertEquals(0, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToGram(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToGram("two");
    }
}
