<?php

namespace Tests\WeightTests\KilogramTests;

class WeightKilogramToMilligramConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConvertInteger()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToMilligram(2);

        $this->assertEquals(2000000, $result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToMilligram(2.5);

        $this->assertEquals(2500000, $result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToMilligram(0);

        $this->assertEquals(0, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToMilligram(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToMilligram("two");
    }
}
