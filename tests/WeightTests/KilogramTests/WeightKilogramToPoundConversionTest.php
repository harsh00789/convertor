<?php

namespace Tests\WeightTests\KilogramTests;

class WeightKilogramToPoundConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConvertInteger()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToPound(2);

        $this->assertEquals(4.4092488404, $result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToPound(2.5);

        $this->assertEquals(5.5115610505, $result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToPound(0);

        $this->assertEquals(0, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToPound(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToPound("two");
    }
}
