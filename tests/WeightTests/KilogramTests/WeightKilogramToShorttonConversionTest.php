<?php

namespace Tests\WeightTests\KilogramTests;

class WeightKilogramToShorttonConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConvertInteger()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToShortton(2);

        $this->assertEquals(0.0022046244, $result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToShortton(2.5);

        $this->assertEquals(0.0027557805, $result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToShortton(0);

        $this->assertEquals(0, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToShortton(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToShortton("two");
    }
}
