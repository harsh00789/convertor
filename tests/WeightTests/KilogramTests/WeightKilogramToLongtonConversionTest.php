<?php

namespace Tests\WeightTests\KilogramTests;

class WeightKilogramToLongtonConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConvertInteger()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToLongton(2);

        $this->assertEquals(0.0019684147, $result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToLongton(2.5);

        $this->assertEquals(0.0024605183, $result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToLongton(0);

        $this->assertEquals(0, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToLongton(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Weight\Kilogram\Kilogram();

        $result = $operation->KilogramToGram("two");
    }
}
