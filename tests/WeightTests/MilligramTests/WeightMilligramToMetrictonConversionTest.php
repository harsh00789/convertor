<?php

namespace Tests\WeightTests\MilligramTests;

class WeightMilligramToMetrictonConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConvertInteger()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToMetricton(2);

        $this->assertEquals(1.999999999E-9, $result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToMetricton(2.5);

        $this->assertEquals(2.499999999E-9, $result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToMetricton(0);

        $this->assertEquals(0, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToMetricton(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToMetricton("two");
    }
}
