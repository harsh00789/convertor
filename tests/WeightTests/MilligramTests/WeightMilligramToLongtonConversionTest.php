<?php

namespace Tests\WeightTests\MilligramTests;

class WeightMilligramToLongtonConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConvertInteger()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToLongton(2);

        $this->assertEquals(1.96841466E-9, $result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToLongton(2.5);

        $this->assertEquals(2.460518326E-9, $result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToLongton(0);

        $this->assertEquals(0, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToLongton(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToGram("two");
    }
}
