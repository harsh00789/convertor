<?php

namespace Tests\WeightTests\MilligramTests;

class WeightMilligramToGramConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConvertInteger()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToGram(2);

        $this->assertEquals(0.002, $result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToGram(2.5);

        $this->assertEquals(0.0025, $result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToGram(0);

        $this->assertEquals(0, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToGram(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToGram("two");
    }
}
