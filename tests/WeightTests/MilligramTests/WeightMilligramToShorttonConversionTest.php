<?php

namespace Tests\WeightTests\MilligramTests;

class WeightMilligramToShorttonConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConvertInteger()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToShortton(2);

        $this->assertEquals(2.20462442E-9, $result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToShortton(2.5);

        $this->assertEquals(2.755780525E-9, $result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToShortton(0);

        $this->assertEquals(0, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToShortton(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToShortton("two");
    }
}
