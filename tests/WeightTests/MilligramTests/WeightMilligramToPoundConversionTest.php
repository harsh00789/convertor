<?php

namespace Tests\WeightTests\MilligramTests;

class WeightMilligramToPoundConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanConvertInteger()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToPound(2);

        $this->assertEquals(0.0000044092, $result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToPound(1);

        $this->assertEquals(0.0000022046, $result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToPound(0);

        $this->assertEquals(0, $result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToPound(null);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Weight\Milligram\Milligram();

        $result = $operation->MilligramToPound("two");
    }
}
