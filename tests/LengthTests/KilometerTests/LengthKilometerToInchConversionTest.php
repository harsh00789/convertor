<?php

namespace Tests\LengthTest\KilometerTests;

class LengthKilometerToInchConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCovertInteger()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToInch(2);

        $this->assertEquals(78740.15748,$result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToInch(2.5);

        $this->assertEquals(98425.19685,$result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToInch(0);

        $this->assertEquals(0,$result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToInch(null);

        
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToInch("two");

        
    }
}