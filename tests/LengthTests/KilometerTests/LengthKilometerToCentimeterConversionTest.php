<?php

namespace Tests\LengthTests\KilometerTests;

class LengthKilometerToCentimeterConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCovertInteger()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToCentimeter(2);

        $this->assertEquals(200000,$result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToCentimeter(2.5);

        $this->assertEquals(250000,$result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToCentimeter(0);

        $this->assertEquals(0,$result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToCentimeter(null);

        
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToCentimeter("two");

        
    }
}