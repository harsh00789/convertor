<?php

namespace Tests\LengthTest\KilometerTests;

class LengthKilometerToMileConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCovertInteger()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToMile(2);

        $this->assertEquals(1.2427377513,$result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToMile(2.5);

        $this->assertEquals(1.5534221891,$result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToMile(0);

        $this->assertEquals(0,$result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToMile(null);

        
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToMile("two");

        
    }
}