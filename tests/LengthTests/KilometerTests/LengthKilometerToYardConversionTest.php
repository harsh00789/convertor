<?php

namespace Tests\LengthTest\KilometerTests;

class LengthKilometerToYardConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCovertInteger()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToYard(2);

        $this->assertEquals(2187.2265967,$result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToYard(2.5);

        $this->assertEquals(2734.0332458,$result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToYard(0);

        $this->assertEquals(0,$result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToYard(null);

        
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToYard("two");

        
    }
}