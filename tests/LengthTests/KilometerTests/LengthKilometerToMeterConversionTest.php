<?php

namespace Tests\LengthTest\KilometerTests;

class LengthKilometerToMeterConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCovertInteger()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToMeter(2);

        $this->assertEquals(2000,$result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToMeter(2.5);

        $this->assertEquals(2500,$result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToMeter(0);

        $this->assertEquals(0,$result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToMeter(null);

        
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Length\Kilometer\Kilometer();

        $result = $operation->KilometerToMeter("two");

        
    }
}