<?php

namespace Tests\LengthTest\CentimeterTests;

class LengthCentimeterToInchConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCovertInteger()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToInch(2);

        $this->assertEquals(0.7874015748,$result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToInch(2.5);

        $this->assertEquals(0.9842519685,$result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToInch(0);

        $this->assertEquals(0,$result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToInch(null);

        
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToInch("two");

        
    }
}