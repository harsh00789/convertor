<?php

namespace Tests\LengthTest\CentimeterTests;

class LengthCentimeterToMeterConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCovertInteger()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToMeter(2);

        $this->assertEquals(0.02,$result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToMeter(2.5);

        $this->assertEquals(0.025,$result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToMeter(0);

        $this->assertEquals(0,$result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToMeter(null);

        
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToMeter("two");

        
    }
}