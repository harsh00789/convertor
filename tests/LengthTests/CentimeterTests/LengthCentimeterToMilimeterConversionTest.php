<?php

namespace Tests\LengthTest\CentimeterTests;

class LengthCentimeterToMillimeterConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCovertInteger()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToMillimeter(2);

        $this->assertEquals(20,$result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToMillimeter(2.5);

        $this->assertEquals(25,$result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToMillimeter(0);

        $this->assertEquals(0,$result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToMillimeter(null);

        
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToMillimeter("two");

        
    }
}