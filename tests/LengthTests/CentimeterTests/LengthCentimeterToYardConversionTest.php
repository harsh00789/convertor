<?php

namespace Tests\LengthTest\CentimeterTests;

class LengthCentimeterToYardConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCovertInteger()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToYard(2);

        $this->assertEquals(0.021872266,$result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToYard(2.5);

        $this->assertEquals(0.0273403325,$result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToYard(0);

        $this->assertEquals(0,$result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToYard(null);

        
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToYard("two");

        
    }
}