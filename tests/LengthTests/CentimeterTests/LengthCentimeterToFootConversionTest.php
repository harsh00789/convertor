<?php

namespace Tests\LengthTest\CentimeterTests;

class LengthCentimeterToFootConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCovertInteger()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToFoot(2);

        $this->assertEquals(0.065616798000000004,$result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToFoot(2.5);

        $this->assertEquals(0.0820209974,$result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToFoot(0);

        $this->assertEquals(0,$result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToFoot(null);

        
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Length\Centimeter\Centimeter();

        $result = $operation->CentimeterToFoot("two");

        
    }
}