<?php

namespace Tests\LengthTest\MeterTests;

class LengthMeterToMillimeterConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCovertInteger()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToMillimeter(2);

        $this->assertEquals(2000,$result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToMillimeter(2.5);

        $this->assertEquals(2500,$result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToMillimeter(0);

        $this->assertEquals(0,$result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToMillimeter(null);

        
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToMillimeter("two");

        
    }
}