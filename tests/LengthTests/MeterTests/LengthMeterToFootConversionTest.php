<?php

namespace Tests\LengthTest\MeterTests;

class LengthMeterToFootConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCovertInteger()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToFoot(2);

        $this->assertEquals(6.56167979,$result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToFoot(2.5);

        $this->assertEquals(8.2020997375,$result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToFoot(0);

        $this->assertEquals(0,$result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToFoot(null);

        
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToFoot("two");

        
    }
}