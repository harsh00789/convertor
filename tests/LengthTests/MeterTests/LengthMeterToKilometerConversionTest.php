<?php

namespace Tests\LengthTest\MeterTests;

class LengthMeterToKilometerConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCovertInteger()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToKilometer(2);

        $this->assertEquals(0.002,$result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToKilometer(2.5);

        $this->assertEquals(0.0025,$result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToKilometer(0);

        $this->assertEquals(0,$result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToKilometer(null);

        
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToKilometer("two");

        
    }
}