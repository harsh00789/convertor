<?php

namespace Tests\LengthTest\MeterTests;

class LengthMeterToYardConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCovertInteger()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToYard(2);

        $this->assertEquals( 2.1872265966,$result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToYard(2.5);

        $this->assertEquals(2.7340332458,$result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToYard(0);

        $this->assertEquals(0,$result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToYard(null);

        
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToYard("two");

        
    }
}