<?php

namespace Tests\LengthTest\MeterTests;

class LengthMeterToCentimeterConversionTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCovertInteger()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToCentimeter(2);

        $this->assertEquals(200,$result);
    }
    public function testCanConvertDouble()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToCentimeter(2.5);

        $this->assertEquals(250,$result);
    }

    public function testCanConvertZero()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToCentimeter(0);

        $this->assertEquals(0,$result);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNull()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToCentimeter(null);

        
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testShouldThrowExceptionIfInputIsNotANumber()
    {
        $operation = new \Convertor\Length\Meter\Meter();

        $result = $operation->MeterToCentimeter("two");

        
    }
}