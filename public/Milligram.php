<!DOCTYPE html>
<html>
<head>
	<title>Milligram Conversion</title>
    <link rel="shortcut icon" href="img/Converter.png" type="image/x-icon" />
</head>
<body>

<form action="" method="GET" >
<center>
<h1><i><img src="img/Converter.png" height=25 width=25>UnitConversion</i><hr></h1>
    Enter the value:<input type="text" name="num1">
</form>

<?php
require '../vendor/autoload.php';
use Convertor\Weight\Milligram\Milligram as Milligram;

$num1 = $_GET["num1"];

        $weight = new Milligram;
try {
    echo "<br><br>MilligramToGram:  ",$weight->MilligramToGram($num1);
        
    echo "<br><br>MilligramToLongton:  ",$weight->MilligramToLongton($num1);
        
    echo "<br><br>MilligramToShortton:  ",$weight->MilligramToShortton($num1);
        
    echo "<br><br>MilligramToPound:  ",$weight->MilligramToPound($num1);
        
    echo "<br><br>MilligramToKilogram:  ",$weight->MilligramToKilogram($num1);
        
    echo "<br><br>MilligramToMetricton:  ",$weight->MilligramToMetricton($num1);
} catch (\InvalidArgumentException $e) {
    echo $e->getMessage(),"\n";
}

?>

</center>
</body>
</html>