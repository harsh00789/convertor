<!DOCTYPE html>
<html>
<head>
	<title>Centimeter Conversion</title>
	        <link rel="shortcut icon" href="img/Converter.png" type="image/x-icon" />
</head>
<body>
<center>
<form action="" method="GET" >

<h1><i><img src="img/Converter.png" height=25 width=25>UnitConversion</i><hr></h1>
    Enter the value:<input type="text" name="num1">
</form>

<?php
require '../vendor/autoload.php';
use Convertor\Length\Centimeter\Centimeter as Centimeter;

$num1 = $_GET["num1"];

        $length = new Centimeter;
try {
    echo "<br><br>CentimeterToMeter:  ",$length->CentimeterToMeter($num1);
    echo "<br><br>CentimeterToFoot:  ",$length->CentimeterToFoot($num1);
    echo "<br><br>CentimeterToInch:  ",$length->CentimeterToInch($num1);
    echo "<br><br>CentimeterToKilometer:  ",$length->CentimeterToKilometer($num1);
    echo "<br><br>CentimeterToMile:  ",$length->CentimeterToMile($num1);
    echo "<br><br>CentimeterToMillimeter:  ",$length->CentimeterToMillimeter($num1);
    echo "<br><br>CentimeterToYard:  ",$length->CentimeterToYard($num1);
} catch (\InvalidArgumentException $e) {
    echo $e->getMessage(),"\n";
}

?>

</center>
</body>
</html>