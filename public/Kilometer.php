<!DOCTYPE html>
<html>
<head>
    <title>Kilometer Conversion</title>
            <link rel="shortcut icon" href="img/Converter.png" type="image/x-icon" />
</head>
<body>
<center>
<form action="" method="GET" >
<h1><i><img src="img/Converter.png" height=25 width=25>UnitConversion</i><hr></h1>
    Enter the value:<input type="text" name="num1">
</form>

<?php
require '../vendor/autoload.php';
use Convertor\Length\Kilometer\Kilometer as Kilometer;

$num1 = $_GET["num1"];

        $length = new Kilometer;
try {
    echo "<br><br>KilometerToMeter:  ",$length->KilometerToMeter($num1);
    echo "<br><br>KilometerToFoot:  ",$length->KilometerToFoot($num1);
    echo "<br><br>KilometerToInch:  ",$length->KilometerToInch($num1);
    echo "<br><br>KilometerToCentimeter:  ",$length->KilometerToCentimeter($num1);
    echo "<br><br>KilometerToMile:  ",$length->KilometerToMile($num1);
    echo "<br><br>KilometerToMillimeter:  ",$length->KilometerToMillimeter($num1);
    echo "<br><br>KilometerToYard:  ",$length->KilometerToYard($num1);
} catch (\InvalidArgumentException $e) {
    echo $e->getMessage(),"\n";
}

?>

</center>
</body>
</html>