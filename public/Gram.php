<!DOCTYPE html>
<html>
<head>
	<title>Gram Conversion</title>
	        <link rel="shortcut icon" href="img/Converter.png" type="image/x-icon" />
</head>
<body>

<form action="" method="GET" >
<center>
<h1><i><img src="img/Converter.png" height=25 width=25>UnitConversion</i><hr></h1>
    Enter the value:<input type="text" name="num1">
</form>

<?php
require '../vendor/autoload.php';
use Convertor\Weight\Gram\Gram as Gram;

$num1 = $_GET["num1"];

        $weight = new Gram;
try {
    echo "<br><br>GramToKilogram:  ",$weight->GramToKilogram($num1);
    echo "<br><br>GramToLongton:  ",$weight->GramToLongton($num1);
    echo "<br><br>GramToShortton:  ",$weight->GramToShortton($num1);
    echo "<br><br>GramToPound:  ",$weight->GramToPound($num1);
    echo "<br><br>GramToMilligram:  ",$weight->GramToMilligram($num1);
    echo "<br><br>GramToMetricton:  ",$weight->GramToMetricton($num1);
} catch (\InvalidArgumentException $e) {
    echo $e->getMessage(),"\n";
}

?>

</center>
</body>
</html>