<!DOCTYPE html>
<html>
<head>
    <title>Meter Conversion</title>
            <link rel="shortcut icon" href="img/Converter.png" type="image/x-icon" />
</head>
<body>
<center>
<form action="" method="GET" >
<h1><i><img src="img/Converter.png" height=25 width=25>UnitConversion</i><hr></h1>
    Enter the value:<input type="text" name="num1">
</form>

<?php
require '../vendor/autoload.php';
use Convertor\Length\Meter\Meter as Meter;

$num1 = $_GET["num1"];

        $length = new Meter;
try {
    echo "<br><br>MeterToKilometer:  ",$length->MeterToKilometer($num1);
    echo "<br><br>MeterToFoot:  ",$length->MeterToFoot($num1);
    echo "<br><br>MeterToInch:  ",$length->MeterToInch($num1);
    echo "<br><br>MeterToMeter:  ",$length->MeterToCentimeter($num1);
    echo "<br><br>MeterToMile:  ",$length->MeterToMile($num1);
    echo "<br><br>MeterToMillimeter:  ",$length->MeterToMillimeter($num1);
    echo "<br><br>MeterToYard:  ",$length->MeterToYard($num1);
} catch (\InvalidArgumentException $e) {
    echo $e->getMessage(),"\n";
}

?>

</center>
</body>
</html>