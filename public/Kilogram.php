<!DOCTYPE html>
<html>
<head>
	<title>Kilogram Conversion</title>
	        <link rel="shortcut icon" href="img/Converter.png" type="image/x-icon" />
</head>
<body>

<form action="" method="GET" >
<center>
<h1><i><img src="img/Converter.png" height=25 width=25>UnitConversion</i><hr></h1>
    Enter the value:<input type="text" name="num1">
</form>

<?php
require '../vendor/autoload.php';
use Convertor\Weight\Kilogram\Kilogram as Kilogram;

$num1 = $_GET["num1"];

        $weight = new Kilogram;
try {
    echo "<br><br>KilogramToGram:  ",$weight->KilogramToGram($num1);
    echo "<br><br>KilogramToLongton:  ",$weight->KilogramToLongton($num1);
    echo "<br><br>KilogramToShortton:  ",$weight->KilogramToShortton($num1);
    echo "<br><br>KilogramToPound:  ",$weight->KilogramToPound($num1);
    echo "<br><br>KilogramToMilliKilogram:  ",$weight->KilogramToMilligram($num1);
    echo "<br><br>KilogramToMetricton:  ",$weight->KilogramToMetricton($num1);
} catch (\InvalidArgumentException $e) {
    echo $e->getMessage(),"\n";
}

?>

</center>
</body>
</html>