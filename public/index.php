<html>
   <head>
      <title>UnitConversion</title>
      <link rel="shortcut icon" href="img/Converter.png" type="image/x-icon" />
   </head>
   <script type="text/javascript">
      /* When the user clicks on the button, 
      toggle between hiding and showing the dropdown content */
      function myWeight() {
      document.getElementById("myDropdown").classList.toggle("shows");
      }
      
      // Close the dropdown menu if the user clicks outside of it
      window.onclick = function(event) {
      if (!event.target.matches('.dropbtn')) {
      
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('shows')) {
      openDropdown.classList.remove('shows');
      }
      }
      }
      }
   </script>
   <script type="text/javascript">
      /* When the user clicks on the button, 
      toggle between hiding and showing the length content */
      function myLength() {
        document.getElementById("mylength").classList.toggle("show");
      }
      
      // Close the length menu if the user clicks outside of it
      window.onclick = function(event) {
      if (!event.target.matches('.lengthbnt')) {
      
        var lengths = document.getElementsByClassName("length-content");
        var i;
        for (i = 0; i < lengths.length; i++) {
          var openlength = lengths[i];
          if (openlength.classList.contains('show')) {
            openlength.classList.remove('show');
          }
        }
      }
      }
   </script>
   <body>
      <style type="text/css">
         /* Dropdown Button */
         .dropbtn {
         background-color: #4CAF50;
         color: white;
         padding: 16px;
         font-size: 16px;
         border: none;
         cursor: pointer;
         }
         /* Dropdown button on hover & focus */
         .dropbtn:hover, .dropbtn:focus {
         background-color: #3e8e41;
         }
         /* The container 
         <div>
         - needed to position the dropdown content */
         .dropdown {
         position: relative;
         display: inline-block;
         }
         /* Dropdown Content (Hidden by Default) */
         .dropdown-content {
         display: none;
         position: absolute;
         background-color: #f9f9f9;
         min-width: 160px;
         box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
         }
         /* Links inside the dropdown */
         .dropdown-content a {
         color: black;
         padding: 12px 16px;
         text-decoration: none;
         display: block;
         }
         /* Change color of dropdown links on hover */
         .dropdown-content a:hover {background-color: #f1f1f1}
         /* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
         .shows {display:block;}
      </style>
      <style type="text/css">
         /* length Button */
         .lengthbnt {
         background-color: #4CAF50;
         color: white;
         padding: 16px;
         font-size: 16px;
         border: none;
         cursor: pointer;
         }
         /* length button on hover & focus */
         .lengthbnt:hover, .lengthbnt:focus {
         background-color: #3e8e41;
         }
         /* The container 
         <div>
         - needed to position the length content */
         .length {
         position: relative;
         display: inline-block;
         }
         /* length Content (Hidden by Default) */
         .length-content {
         display: none;
         position: absolute;
         background-color: #f9f9f9;
         min-width: 160px;
         box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
         }
         /* Links inside the length */
         .length-content a {
         color: black;
         padding: 12px 16px;
         text-decoration: none;
         display: block;
         }
         /* Change color of length links on hover */
         .length-content a:hover {background-color: #f1f1f1}
         /* Show the length menu (use JS to add this class to the .length-content container when the user clicks on the length button) */
         .show {display:block;}
      </style>
      <center>
         <h1>
            <i><img src="img/Converter.png" height=25 width=25>UnitConversion</i>
            <hr>
         </h1>
         <div class="dropdown">
            <button onclick="myWeight()" class="dropbtn">Weight</button>
            <div id="myDropdown" class="dropdown-content">
               <a href="Gram.php?num1">Gram</a>
               <a href="Kilogram.php">Kilogram</a>
               <a href="Milligram.php">Milligram</a>
            </div>
         </div>
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <div class="length">
            <button onclick="myLength()" class="lengthbnt">Length</button>
            <div id="mylength" class="length-content">
               <a href="Centimeter.php">Centimeter</a>
               <a href="Kilometer.php">Kilometer</a>
               <a href="Meter.php">Meter</a>
            </div>
         </div>
      </center>
   </body>
</html>